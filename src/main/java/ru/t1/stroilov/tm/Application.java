package ru.t1.stroilov.tm;

import static ru.t1.stroilov.tm.constant.AppConstant.*;

public class Application {

    public static void main(String[] args) {
        showWelcome();
        runApplication(args);
        System.exit(0);
    }

    private static void runApplication(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        switch (arg) {
            case VERSION:
                showVersion();
                break;
            case INFO:
                showDeveloperInfo();
                break;
            case HELP:
                showHelp();
                break;
            default:
                showUnknownCommand(arg);
                break;
        }
    }

    private static void showWelcome() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf(
                "%s - Display program version.\n" +
                "%s - Display developer info.\n" +
                "%s - Display list of terminal commands.\n",
                VERSION, INFO, HELP
        );
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.3.0");
    }

    private static void showDeveloperInfo() {
        System.out.println("[DEVELOPER INFORMATION]");
        System.out.println("Stroilov Ivan");
        System.out.println("ivanstroilov@gmail.com");
    }

    private static void showUnknownCommand(final String arg) {
        System.out.printf("Command '%s' not supported.\n", arg);
    }

}
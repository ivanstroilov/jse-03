package ru.t1.stroilov.tm.constant;

public final class AppConstant {

    public static final String VERSION = "version";

    public static final String HELP = "help";

    public static final String INFO = "info";

}
